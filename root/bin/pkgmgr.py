import os, sys, json

def pkgmgr():
    load()
    clear()
    print "X-Points:", allvar['points']
    print "\nAvailable Packages:"
    print "quiz - A simple quiz game (Free)"
    print "adventure - A simple text-based adventure game (Free)"
    print "xedit - A simple text editor (5 X-Points)"
    if allvar['edit'] == 1:
        print "ycontact - Contact another user. Uses temporary implementation of yNet. (10 X-Points)"
        print "name - Give yourself a name (7 X-Points)"
    if allvar['name1'] == 1:
        print "xkernel2 - Update of the kernel. Supports internet. (50 X-Points)"
        print "adventure2 - Sequal to adventure (10 X-Points)"
    if allvar['xkernel2'] == 1:
        print "xstorage - Allows for mounting extra storage devices (20 X-Points)"
        print "yglobe - yGlobe Simple yNet Browser (100 X-Points)"
    if allvar['update1'] == 1:
        print "update1 - XYZ Update Package"
    cinput("Type what you want >> ")
    
    if temp == "quiz":
        if allvar['quiz'] == 1:
            print "You already bought this"
        else:
            allvar['quiz'] = 1
            print "You Bought 'quiz'!"
        
    if temp == "adventure":
        if allvar['adventure'] == 1:
            print "You already bought this"
        else:
            allvar['adventure'] = 1
            print "You Bought 'adventure'!"
        
    if temp == "xedit":
        if allvar['edit'] == 1:
            print "You already bought this"
        elif allvar['points'] < 5:
            print "Insuffencient funds"
        elif allvar['edit'] == 0:
            allvar['edit'] = 1
            allvar['points'] -= 5
            print "You bought 'xedit'!"
            
    if temp == "ycontact":
        if allvar['edit'] == 1:
            if allvar['contact'] == 1:
                print "You already bought this"
            elif allvar['points'] < 10:
                print "Insuffencient funds"
            elif allvar['contact'] == 0:
                allvar['contact'] = 1
                allvar['points'] -= 10
                print "You bought ycontact!"
            
    if temp == "name":
        if allvar['edit'] == 1:
            if allvar['name1'] == 1:
                print "You already bought this"
            elif allvar['points'] < 7:
                print "Insuffencient funds"
            elif allvar['name1'] == 0:
                allvar['name1'] = 1
                allvar ['points'] -= 10
                print "You bought name!"
            
    if temp == "xkernel2":
        if allvar['name1'] == 1:
            if allvar['xkernel2'] == 1:
                print "You already bought this"
            elif allvar['points'] < 50:
                print "Insuffencient funds"
            elif allvar['xkernel2'] == 0:
                allvar['xkernel2'] = 1
                allvar ['points'] -= 50
                print "You bought xkernel2!"
            
    if temp == "adventure2":
        if allvar['name1'] == 1:
            if allvar['adventure2'] == 1:
                print "You already bought this"
            if allvar['points'] < 10:
                print "Insuffencient funds"
            elif allvar['adventure2'] == 0:
                allvar['adventure2'] = 1
                allvar ['points'] -= 10
                print "You bought adventure2!"
            
    if temp == "yglobe":
        if allvar['xkernel2'] == 1:
            if allvar['xglobe'] == 1:
                print "You already bought this"
            if allvar['points'] < 100:
                print "Insuffencient funds"
            elif allvar['xglobe'] == 0:
                allvar['xglobe'] = 1
                allvar ['points'] -= 100
                print "You bought yglobe!"
            
    if temp == "update1":
        if allvar['update1'] == 1:
            clear()
            print """WARNING!
THIS PIECE OF SOFTWARE COULD POTENTIALLY BE UNSTABLE.
USUALLY UNSTABLE PACKAGES ARE NOT PUSHED TO THE PACKAGE MANAGER,
BUT WE WERE ASSURED IT WOULDN'T DO ANY HARM.
AFTER MUCH TESTING, IT SHOULDN'T DO MUCH HARM AS LONG AS YOU INSTALL IT THE RIGHT WAY.
IF YOU ARE READY, GOTO THE INSTALLER.

>install"""
            dinput()
            if temp == "install":
                allvar['update1'] = 2
                print "Success!"
    save()

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()